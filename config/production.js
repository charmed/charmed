const apiOrigin = 'https://api.charmed.cloud';

module.exports = {
  useSampleClass: false,
  isProduction: true,
  uri: {
    classes: `${apiOrigin}/classes`,
    feedback: `${apiOrigin}/feedback`,
  },
};
