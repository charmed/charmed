const apiOrigin = 'http://localhost:8080';

module.exports = {
  useSampleClass: false,
  defaultRoute: '/',
  isProduction: false,
  uri: {
    classes: "/api/classes",
  },
};
