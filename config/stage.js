const apiOrigin = 'https://api.stage.charmed.cloud';

module.exports = {
  useSampleClass: false,
  isProduction: false,
  uri: {
    classes: `${apiOrigin}/classes`,
    feedback: `${apiOrigin}/feedback`,
  },
};
