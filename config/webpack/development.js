const {merge} = require('webpack-merge');
const common = require('./common');

module.exports = merge(common, {
  mode: 'development',
  stats: 'errors-only',
  devtool: 'source-map',
  devServer: {
    compress: true,
    historyApiFallback: true,
    port: 8080,
    headers: {'Access-Control-Allow-Origin': '*'},
    proxy: {
      '/api': {
        target: 'https://api.stage.charmed.cloud',
        pathRewrite: {'^/api': ''},
        secure: false,
        changeOrigin: true,
      },
    },
  },
  performance: {
    hints: false,
  },
});
