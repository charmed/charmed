const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

module.exports = {
  entry: {
    app: path.resolve(__dirname, '../../src/index.js'),
    'service-worker': path.resolve(__dirname, '../../src/auth/service-worker.js'),
  },
  output: {
    filename: chunkData => (chunkData.chunk.name === 'service-worker' ? '[name].js' : '[name].[chunkhash].js'),
    hashDigestLength: 10,
    path: path.resolve(__dirname, '../../dist'),
  },
  module: {
    rules: [
      {
        test: /src.*\.js$/,
        use: [{
          loader: "babel-loader",
          options: {
            presets: ['@babel/preset-env', '@babel/preset-react'],
          }
        }],
      },
      {
        test: /\.(scss)$/,
        use: [
          {loader: 'style-loader'},
          {loader: 'css-loader'},
          {loader: 'sass-loader'},
        ],
      },
      {
        test: /\.(gif|svg|jpg|png)$/,
        loader: 'file-loader',
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      favicon: path.resolve(__dirname, '../../src/assets/template/favicon.ico'),
      template: path.resolve(__dirname, '../../src/assets/template/index.html'),
    }),
  ],
};
