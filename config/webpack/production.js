const {merge} = require('webpack-merge');
const common = require('./common');

module.exports = merge(common, {
  mode: 'production',
  optimization: {
    splitChunks: {
      cacheGroups: {
        bootstrap: {
          test: /[\\/]node_modules[\\/](boot|react)strap/,
          name: 'bootstrap',
        },
        lodash: {
          test: /[\\/]node_modules[\\/]lodash/,
          name: 'lodash',
        },
      },
    },
  },
});
