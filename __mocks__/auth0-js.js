
class WebAuth {
  authorize = jest.fn();
  logout = jest.fn();
  parseHash = jest.fn(({ hash }, callback) => {
    if (hash) {
      callback(undefined, { accessToken: hash });
    }
    else {
      callback(new Error('failed'));
    }
  });
}

export default {
  WebAuth,
};
