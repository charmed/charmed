import _chunk from 'lodash/chunk';

// Just chunks the total into groups of targetGroupSize
export const determineGroupSizes = jest.fn((total, targetGroupSize) => {
  const result = [];
  while (total >= targetGroupSize) {
    result.push(targetGroupSize);
    total -= targetGroupSize; // eslint-disable-line
  }
  if (total > 0) {
    result.push(total);
  }
  return result;
});

// Returns an array where each group has one ID in it
export default jest.fn(ids => _chunk(ids));
