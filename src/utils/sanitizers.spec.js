import {onlyAlphaAndSpaces} from './sanitizers';

describe('sanitizers', () => {
  describe('onlyAlphaAndSpaces', () => {
    it.each([
      ['blah', 'blah'],
      ['blah1', 'blah'],
      [' blah1', ' blah'],
      ['bl  ah', 'bl  ah'],
      ['blah 1and', 'blah and'],
      ['a87ghd3awgw3c0a3cw65cb3', 'aghdawgwcacwcb'],
      ['Q&67*#Q6*)qQ&Y2^x', 'QQqQYx'],
    ])('%s -> %s', (input, expectedOutput) => {
      expect(onlyAlphaAndSpaces(input)).toEqual(expectedOutput);
    });
  });
});
