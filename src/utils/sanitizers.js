/* eslint-disable no-useless-escape */

export const onlyAlphaAndSpaces = s => s.replace(/[^a-zA-Z ]/g, '');
export const onlyAlphaSpacesAndNewlines = s => s.replace(/[^a-zA-Z \n]/g, '');
export const onlyAlphaSpacesPunctuationAndNumbers = s => s.replace(/[^a-zA-Z,!"'\[\]\(\);.\-:\d\/ ]/g, '');

export default onlyAlphaAndSpaces;
