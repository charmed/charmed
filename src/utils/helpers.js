/* eslint-disable no-useless-escape, max-len */

// sort by IDs
export const caseInsensitiveIdSort = (a, b) => a.id.toLowerCase() < b.id.toLowerCase() ? -1 : 1;


// sort first by names, then by IDs if names are same
export const caseInsensitiveNameSort = (a, b) => {
  const aName = a.name.toLowerCase();
  const bName = b.name.toLowerCase();

  if (aName === bName) {
    return caseInsensitiveIdSort(a, b);
  }
  return aName < bName ? -1 : 1;
};

export const isValidEmail = email =>
  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email)
;
