// import URI from '@utils/uri';
// import { mockStore, mockApi } from './setup';
// import { classListActions as actions } from '@actions/types';
// import {
//   fetchClassList,
//   createClass,
//   updateClass,
//   deleteClass } from '@actions/class/list';
// import { closeModal } from '@actions/class/modal';
// import {
//   openClass,
//   openLocalClass,
//   closeClass } from '@actions/class/active';
// import { STATUS } from '@reducers/user';

// let store;
// beforeEach(() => {
//   store = mockStore();
//   mockApi.reset();
// });

// jest.mock('@actions/class/modal');
// jest.mock('@actions/class/active');
// afterEach(() => {
//   closeModal.mockClear();
//   OPEN_CLASS.mockClear();
//   openLocalClass.mockClear();
//   closeClass.mockClear();
// });

// const { arrayContaining, objectContaining } = expect;
// describe('fetchClassList', () => {
//   it('success - empty', async () => {
//     mockApi.onGet(URI.classes).reply(200);

//     const expected = [
//       {
//         type: model.FETCHING_CLASSES,
//       },
//       {
//         type: model.SET_CLASSES,
//         fetchedClassList: [],
//       },
//     ];

//     await store.dispatch(model.fetchClassList());
//     const actual = store.getActions();
//     expect(actual).toEqual(expected);
//   });

//   it('success - populated', async () => {
//     const fetchedClassList = [{ classId: 'blah', name: 'bloo', stuents: [] }];
//     mockApi.onGet(URI.classes).reply(200, fetchedClassList);

//     const expected = [
//       {
//         type: model.FETCHING_CLASSES,
//       },
//       {
//         type: model.SET_CLASSES,
//         fetchedClassList,
//       },
//     ];

//     await store.dispatch(model.fetchClassList());
//     const actual = store.getActions();
//     expect(actual).toEqual(expected);
//   });

//   it('failed fetch', async () => {
//     mockApi.onGet(URI.classes).reply(400);

//     const expected = [
//       {
//         type: model.FETCHING_CLASSES,
//       },
//       objectContaining({
//         type: model.FETCHING_CLASSES_ERROR,
//       }),
//     ];

//     await store.dispatch(model.fetchClassList());
//     const actual = store.getActions();
//     expect(actual).toEqual(expected);
//   });
// });

// describe('createClass', () => {
//   it('success - authed', async () => {
//     const classInfo = {
//       name: 'blah',
//       students: [{ name: 'john' }],
//     };
//     mockApi.onPost(URI.classes).reply(200);

//     const expected = arrayContaining([
//       objectContaining({
//         type: model.CREATE_CLASS,
//         className: classInfo.name,
//       }),
//     ]);

//     store = mockStore({ user: { status: statusLOGGED_IN } });
//     await store.dispatch(model.createClass(classInfo));
//     const actual = store.getActions();
//     expect(actual).toEqual(expected);
//     expect(closeModal).toBeCalled();
//     expect(openClass).toBeCalled();
//   });

//   it('success - non-authed', () => {
//     const classInfo = {
//       name: 'blah',
//       students: [{ name: 'john' }],
//     };

//     const expected = arrayContaining([
//       objectContaining({
//         type: model.CREATE_CLASS,
//         className: classInfo.name,
//       }),
//     ]);

//     store = mockStore({ user: { status: statusLOGGED_OUT } });
//     store.dispatch(model.createClass(classInfo));
//     const actual = store.getActions();
//     expect(actual).toEqual(expected);
//     expect(closeModal).toBeCalled();
//     expect(openLocalClass).toBeCalled();
//   });

//   it('failed post', async () => {
//     const classInfo = {
//       name: 'blah',
//       students: [{ name: 'john' }],
//     };
//     mockApi.onPost(URI.classes).reply(400);

//     const expected = arrayContaining([
//       objectContaining({
//         type: model.CREATE_CLASS_ERROR,
//       }),
//     ]);

//     store = mockStore({ user: { status: statusLOGGED_IN } });
//     await store.dispatch(model.createClass(classInfo));
//     const actual = store.getActions();
//     expect(actual).toEqual(expected);
//     expect(openClass).not.toBeCalled();
//   });
// });

// describe('updateClass', () => {
//   it('success - authed', async () => {
//     const classInfo = {
//       classId: 1,
//       name: 'blah',
//       students: [{ name: 'john' }],
//     };
//     mockApi.onPut(`${URI.classes}/1`).reply(200);

//     const expected = arrayContaining([
//       objectContaining({
//         type: model.UPDATE_CLASS,
//         classId: classInfo.classId,
//         className: classInfo.name,
//       }),
//     ]);

//     store = mockStore({ user: { status: statusLOGGED_IN } });
//     await store.dispatch(model.updateClass(classInfo));
//     const actual = store.getActions();
//     expect(actual).toEqual(expected);
//     expect(closeModal).toBeCalled();
//     expect(openClass).toBeCalledWith(classInfo.classId);
//   });

//   it('success - non-authed', async () => {
//     const classInfo = {
//       classId: 1,
//       name: 'blah',
//       students: [{ name: 'john' }],
//     };
//     mockApi.onPut(`${URI.classes}/1`).reply(200);

//     const expected = arrayContaining([
//       objectContaining({
//         type: model.UPDATE_CLASS,
//         classId: classInfo.classId,
//         className: classInfo.name,
//       }),
//     ]);

//     store = mockStore({ user: { status: statusLOGGED_OUT } });
//     await store.dispatch(model.updateClass(classInfo));
//     const actual = store.getActions();
//     expect(actual).toEqual(expected);
//     expect(closeModal).toBeCalled();
//     expect(openLocalClass).toBeCalledWith(classInfo);
//   });

//   it('failed put', async () => {
//     const classInfo = {
//       classId: 1,
//       name: 'blah',
//       students: [{ name: 'john' }],
//     };
//     mockApi.onPut(`${URI.classes}/1`).reply(400);

//     const expected = arrayContaining([
//       objectContaining({
//         type: model.UPDATE_CLASS_ERROR,
//       }),
//     ]);

//     store = mockStore({ user: { status: statusLOGGED_IN } });
//     await store.dispatch(model.updateClass(classInfo));
//     const actual = store.getActions();
//     expect(actual).toEqual(expected);
//     expect(openClass).not.toBeCalled();
//   });
// });

// describe('deleteClass', () => {
//   it('success', async () => {
//     const classId = 1;
//     mockApi.onDelete(`${URI.classes}/1`).reply(200);

//     const expected = arrayContaining([
//       objectContaining({
//         type: model.DELETE_CLASS,
//         classId,
//       }),
//     ]);

//     await store.dispatch(model.deleteClass(classId));
//     const actual = store.getActions();
//     expect(actual).toEqual(expected);
//     expect(closeModal).toBeCalled();
//     expect(closeClass).toBeCalled();
//   });

//   it('failed put', async () => {
//     const classId = 1;
//     mockApi.onDelete(`${URI.classes}/1`).reply(400);

//     const expected = arrayContaining([
//       objectContaining({
//         type: model.DELETE_CLASS_ERROR,
//       }),
//     ]);

//     await store.dispatch(model.deleteClass(classId));
//     const actual = store.getActions();
//     expect(actual).toEqual(expected);
//     expect(closeClass).not.toBeCalled();
//   });
// });
