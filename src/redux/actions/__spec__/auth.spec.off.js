// import { mockStore, mockApi } from './setup';
// import { userActions as actions } from '@actions/types';
// import { handleLogIn } from '@actions/user';
// import { fetchClassList } from '@actions/class/list';

// let store;
// beforeEach(() => {
//   store = mockStore();
//   mockApi.reset();
// });

// jest.mock('@actions/class/list');
// afterEach(() => {
//   fetchClassList.mockClear();
// });

// const { arrayContaining, objectContaining } = expect;
// describe('handleLogIn', () => {
//   it('success', async () => {
//     await store.dispatch(model.handleLogIn('token'));
//     const expected = arrayContaining([
//       objectContaining({
//         type: model.USER_SET,
//         accessToken: 'token',
//       }),
//     ]);
//     const actual = store.getActions();
//     expect(actual).toEqual(expected);
//     expect(fetchClassList).toBeCalled();
//   });

//   it('fail on token', async () => {
//     // Mock fails if no token passed - simulates bad token
//     await store.dispatch(model.handleLogIn());
//     const expected = [
//       objectContaining({
//         type: model.USER_ERROR,
//         errorMsg: 'Invalid or expired token.',
//       }),
//     ];
//     const actual = store.getActions();
//     expect(actual).toEqual(expected);
//     expect(fetchClassList).not.toBeCalled();
//   });
// });
