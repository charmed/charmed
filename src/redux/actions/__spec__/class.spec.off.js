// import URI from '@utils/uri';
// import { mockStore, mockApi } from './setup';
// import { activeClassActions as actions } from '@actions/types';
// import {
//   openClass,
//   openLocalClass,
//   updateGroupSize,
//   editClass,
//   closeClass,
//   toggleStudentAbsent } from '@actions/class/active';
// import {
//   updateModalName,
//   updateModalStudents,
//   openModal } from '@actions/class/modal';

// let store;
// beforeEach(() => {
//   store = mockStore();
//   mockApi.reset();
// });

// jest.mock('@actions/class/modal');
// afterEach(() => {
//   updateModalName.mockClear();
//   updateModalStudents.mockClear();
//   openModal.mockClear();
// });

// const { objectContaining } = expect;
// describe('updateGroupSize', () => {
//   it('success', () => {
//     const groupSize = 5;
//     store.dispatch(model.updateGroupSize(groupSize));

//     const expected = [{
//       type: model.UPDATE_GROUP_SIZE,
//       groupSize,
//     }];
//     const actual = store.getActions();

//     expect(actual).toEqual(expected);
//   });
// });

// describe('openClass', () => {
//   it('success', async () => {
//     // Setup
//     const initialClassInfo = { classId: '1', x: 3, y: 4 };
//     mockApi.onGet(`${URI.classes}/1`).reply(200, initialClassInfo);

//     const expected = [
//       {
//         type: model.FETCHING_CLASS,
//       },
//       {
//         type: model.UPDATE_GROUP_SIZE,
//         groupSize: 2,
//       },
//       {
//         type: model.OPEN_CLASS,
//         initialClassInfo,
//       },
//     ];

//     await store.dispatch(model.openClass('1'));
//     const actual = store.getActions();
//     expect(actual).toEqual(expected);
//   });

//   it('failed fetch', async () => {
//     // Setup
//     mockApi.onGet(`${URI.classes}/1`).reply(400);

//     const expected = [
//       {
//         type: model.FETCHING_CLASS,
//       },
//       objectContaining({
//         type: model.OPEN_CLASS_ERROR,
//       }),
//     ];

//     await store.dispatch(model.openClass('1'));
//     const actual = store.getActions();
//     expect(actual).toEqual(expected);
//   });
// });

// describe('openLocalClass', () => {
//   it('success', () => {
//     // Setup
//     const initialClassInfo = { classId: '1', x: 3, y: 4 };

//     const expected = [
//       {
//         type: model.UPDATE_GROUP_SIZE,
//         groupSize: 2,
//       },
//       {
//         type: model.OPEN_CLASS,
//         initialClassInfo,
//       },
//     ];

//     store.dispatch(model.openLocalClass(initialClassInfo));
//     const actual = store.getActions();
//     expect(actual).toEqual(expected);
//   });
// });

// describe('editClass', () => {
//   it('success', () => {
//     const classId = 1;
//     const name = 'blah';
//     const students = [
//       { name: 'casey', isPresent: true },
//       { name: 'john', isPresent: true },
//     ];
//     const mappedStudents = ['casey', 'john'];

//     store = mockStore({
//       class: {
//         active: {
//           classId,
//           name,
//           students,
//         },
//       },
//     });

//     store.dispatch(model.editClass());
//     expect(updateModalName).toBeCalledWith(name);
//     expect(updateModalStudents).toBeCalledWith(mappedStudents);
//     expect(openModal).toBeCalledWith(classId);
//   });
// });

// describe('closeClass', () => {
//   it('success', () => {
//     store.dispatch(model.closeClass());

//     const expected = [{
//       type: model.CLOSE_CLASS,
//     }];
//     const actual = store.getActions();

//     expect(actual).toEqual(expected);
//   });
// });

// describe('toggleStudentAbsent', () => {
//   it('success (more than 2 students are present)', () => {
//     const students = [
//       { name: 'casey', isPresent: true },
//       { name: 'john', isPresent: true },
//       { name: 'van', isPresent: true },
//     ];
//     const index = 1;
//     store = mockStore({ class: { active: { students } } });

//     const expected = [{
//       type: model.TOGGLE_STUDENT_ABSENT,
//       index,
//     }];

//     store.dispatch(model.toggleStudentAbsent(index));
//     const actual = store.getActions();
//     expect(actual).toEqual(expected);
//   });
//   it('success (target group size needs to be reduced)', () => {
//     const students = [
//       { name: 'casey', isPresent: true },
//       { name: 'john', isPresent: true },
//       { name: 'van', isPresent: true },
//     ];
//     const index = 1;
//     store = mockStore({
//       class: {
//         active:
//         {
//           students,
//           groupSize: 3,
//         },
//       },
//     });

//     const expected = [{
//       type: model.UPDATE_GROUP_SIZE,
//       groupSize: 2,
//     }, {
//       type: model.TOGGLE_STUDENT_ABSENT,
//       index,
//     }];

//     store.dispatch(model.toggleStudentAbsent(index));
//     const actual = store.getActions();
//     expect(actual).toEqual(expected);
//   });
//   it('success (only 2 students are present)', () => {
//     const students = [
//       { name: 'casey', isPresent: true },
//       { name: 'john', isPresent: false },
//       { name: 'van', isPresent: true },
//     ];
//     const index = 0;
//     store = mockStore({ class: { active: { students } } });

//     const expected = [];

//     store.dispatch(model.toggleStudentAbsent(index));
//     const actual = store.getActions();
//     expect(actual).toEqual(expected);
//   });
//   it('success (only 2 students are present, but student was absent)', () => {
//     const students = [
//       { name: 'casey', isPresent: true },
//       { name: 'john', isPresent: false },
//       { name: 'van', isPresent: true },
//     ];
//     const index = 1;
//     store = mockStore({ class: { active: { students } } });

//     const expected = [{
//       type: model.TOGGLE_STUDENT_ABSENT,
//       index,
//     }];

//     store.dispatch(model.toggleStudentAbsent(1));
//     const actual = store.getActions();
//     expect(actual).toEqual(expected);
//   });
// });
