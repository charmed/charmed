import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

export const mockStore = configureMockStore([thunk]);
export const mockApi = new MockAdapter(axios);
