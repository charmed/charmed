import mockActionCreator from '.';

export const openModal = mockActionCreator();
export const updateModalName = mockActionCreator();
export const updateModalStudents = mockActionCreator();
export const closeModal = mockActionCreator();
