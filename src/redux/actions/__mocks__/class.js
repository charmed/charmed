import mockActionCreator from '.';

export const updateGroupSize = mockActionCreator();
export const openClass = mockActionCreator();
export const openLocalClass = mockActionCreator();
export const editClass = mockActionCreator();
export const closeClass = mockActionCreator();
export const toggleStudentAbsent = mockActionCreator();
export const generateGroups = mockActionCreator();
