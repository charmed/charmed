import mockActionCreator from '.';

export const fetchClassList = mockActionCreator();
export const createClass = mockActionCreator();
export const updateClass = mockActionCreator();
export const deleteClass = mockActionCreator();
