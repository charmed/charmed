export {default as GeneratePage} from './GeneratePage';
export {default as RulesPage} from './RulesPage';
export {default as UnsupportedPage} from './UnsupportedPage';
export {default as WelcomePage} from './WelcomePage';
