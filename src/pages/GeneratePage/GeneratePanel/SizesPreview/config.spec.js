import {getGridDimensions, getGridPosition} from './config';

describe('getGridDimensions', () => {
  const CELL_SIZE = 10;

  it.each([
    // No Max Width
    [Infinity, 1, {rows: 1, cols: 1}],
    [Infinity, 2, {rows: 1, cols: 2}],
    [Infinity, 3, {rows: 1, cols: 3}],
    [Infinity, 4, {rows: 2, cols: 2}],
    [Infinity, 5, {rows: 2, cols: 3}],
    [Infinity, 6, {rows: 2, cols: 3}],
    [Infinity, 7, {rows: 2, cols: 4}],
    [Infinity, 8, {rows: 2, cols: 4}],
    [Infinity, 9, {rows: 3, cols: 3}],
    [Infinity, 10, {rows: 3, cols: 4}],
    [Infinity, 11, {rows: 3, cols: 4}],
    [Infinity, 12, {rows: 3, cols: 4}],
    [Infinity, 13, {rows: 3, cols: 5}],
    [Infinity, 14, {rows: 3, cols: 5}],
    [Infinity, 15, {rows: 3, cols: 5}],
    [Infinity, 16, {rows: 4, cols: 4}],
    // Max Width
    [19, 2, {rows: 2, cols: 1}],
    [19, 3, {rows: 3, cols: 1}],
    [29, 4, {rows: 2, cols: 2}],
    [29, 5, {rows: 3, cols: 2}],
    [39, 16, {rows: 6, cols: 3}],
  ])('(maxWidth = %s, numStudents = %s) -> %s', (maxWidth, numStudents, expected) => {
    expect(getGridDimensions(maxWidth, numStudents, CELL_SIZE, 0)).toEqual(expected);
  });
});

describe('getGridPosition', () => {
  it.each([
    [1, 0, {row: 1, col: 1}],
    [1, 1, {row: 2, col: 1}],
    [2, 0, {row: 1, col: 1}],
    [2, 1, {row: 1, col: 2}],
    [2, 2, {row: 2, col: 1}],
    [2, 3, {row: 2, col: 2}],
    [3, 0, {row: 1, col: 1}],
    [3, 1, {row: 1, col: 2}],
    [3, 2, {row: 1, col: 3}],
    [3, 3, {row: 2, col: 1}],
    [3, 4, {row: 2, col: 2}],
    [3, 5, {row: 2, col: 3}],
    [3, 6, {row: 3, col: 1}],
    [3, 7, {row: 3, col: 2}],
    [3, 8, {row: 3, col: 3}],
    [4, 0, {row: 1, col: 1}],
    [4, 1, {row: 1, col: 2}],
    [4, 2, {row: 1, col: 3}],
    [4, 3, {row: 1, col: 4}],
    [4, 4, {row: 2, col: 1}],
    [4, 5, {row: 2, col: 2}],
    [4, 6, {row: 2, col: 3}],
    [4, 7, {row: 2, col: 4}],
    [4, 8, {row: 3, col: 1}],
    [4, 9, {row: 3, col: 2}],
    [4, 10, {row: 3, col: 3}],
    [4, 11, {row: 3, col: 4}],
    [4, 12, {row: 4, col: 1}],
    [4, 13, {row: 4, col: 2}],
    [4, 14, {row: 4, col: 3}],
    [4, 15, {row: 4, col: 4}],

  ])('(numStudents = %s, numRows = %s, studentIndex = %s) -> %s', (numCols, studentIndex, expectedPosition) => {
    expect(getGridPosition(numCols, studentIndex)).toEqual(expectedPosition);
  });
});
