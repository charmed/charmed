
module.exports = {
  clearMocks: true,
  collectCoverageFrom: ['src/**/*.js'],
  coverageDirectory: 'coverage',
  coveragePathIgnorePatterns: [
    'src/template/',
  ],
  coverageReporters: [
    'text',
    'json',
  ],
  moduleNameMapper: {
    '\\.(jpg|jpeg|png|svg)$': '<rootDir>/__mocks__/fileMock.js',
  },
  roots: ['<rootDir>/src/'],
  testMatch: [
    '**/?(*.)+(spec).js',
  ],
};
