module.exports = {
  parser: "@babel/eslint-parser",
  extends: [
    'defaults/configurations/google',
    'plugin:react/recommended',
    'plugin:jsx-a11y/recommended'
  ],
  parserOptions: {
    ecmaVersion: 2019,
    sourceType: 'module'
  },
  env: {
    node: true,
    es6: true,
    browser: true,
    jest: true
  },
  settings: {
    'import/resolver': 'webpack',
    react: {
      version: 'detect'
    },
  },
  plugins: [
    'react'
  ],
  rules: {
    'brace-style': [ 'error', 'stroustrup', {'allowSingleLine': true}],
    'max-len': ['error', 120],
    'react/prop-types': 'off',
  },
};
