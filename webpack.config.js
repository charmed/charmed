const development = require('./config/webpack/development');
const production = require('./config/webpack/production');

module.exports = () => {
  if (process.env.NODE_ENV === 'production') {
    return production;
  }
  return development;
};
